# __Komputer Store__

## __Overview__

The project is a small web application built from scratch using plain HTML, CSS and JavaScript:<br>

![Example](/resources/images/Screenshot Example.png? "Example")

## __Description__

The application has 3 main parts:

1. __BANK__.

    1. <ins>*Balance*</ins><br>
        The amount of money in the bank account.

    1. <ins>*Outstanding loan*</ins> (only visible after taking a loan)<br>
        The amount of money left to pay back the loan.

    1. <ins>*Get a loan __button__*</ins><br>
        A restriction applies that it is not possible to get a loan more than double the account balance. Clicking on the button attempts to get a loan:
        - If the user has any earnings, it is asked whether the user would like to deposit it in order to get a higher loan.
        - A *prompt* starts with a placeholder value set to maximum possible loan.
        - However, if the loan request is redirected from the shop, the prompt is skipped to avoid unnecessary double confirmation by the user.
        - At this point, the user can confirm the request with the default value or adjust the amount, or cancel the request.
        - Various fringe cases are handled:
            - if the user enters anything else than a number, alert message "*Invalid input*" is shown,
            - if the user enters a value that is greater than the maximum possible, alert *message* is shown, notifying that the request cannot exceed double the account balance. 
            - if there is a current outstanding loan, alert *message* is shown, notifying the user that a new loan cannot be considered before the outstanding one has been paid off (NOTE: the case where the user could request another loan should balance be positive after repaying the existing loan with earnings is not considered). <br>

1. __WORK__.

    1. <ins>*Pay*</ins><br>
        The amount of money that has been earned by working (pressing the *earn button* (*see* below)). It is not a part of the account balance.

    1. <ins>*Deposit __button__*</ins><br>
        Clicking on the button transfers the money from the *Pay balance* to the *Bank balance*. However:
        - If there is an outstanding loan, 10% is first deducted to pay of the loan. The rest is then transferred to the account.

    1. <ins>*Earn __button__*</ins><br>
        Clicking on the button increases the *Pay balance* by 100 on each click.

    1. <ins>*Repay loan __button__*</ins> (only visible after taking a loan)<br>
        Clicking on the button uses the full value of *Pay balance* or the value equal to the *Outstanding loan* - whichever is smaller - to pay off the loan.

1. __LAPTOPS__.

    1. <ins>*Laptop selection section*</ins><br>
        This area is for selecting a laptop of interest to potentially buy it. Currently, all the data about laptops is stored locally, and can be expanded to fetch from a database. The page is loaded with the first laptop from the list stored in memory. Upon selection of a laptop:
        - a quick feature list is provided below the selection box,
        - the information in the main box below is updated with a picture of the laptop, its description and price.

    1. <ins>*Laptop information section*</ins><br>
        As mentioned above, this section contains the picture of the selected laptop (or the one provided on page load), short description and <ins>*Buy __button__*</ins> - the final action of the website. Clicking on the button attempts to purchase the item:
        - If the account balance is large enough, the laptop is purchased, and the user is informed of the purchased item.
        - If the balance is too low:
            - first, it is checked whether there is enough earnings and funds in the bank account to buy the selected laptop without taking out a loan (any outstanding loan and resulting repayments from depositing earnings are accounted for). Sensible and minimal prompts are provided along the way;
            - eventually, maximal possible loan amount is taken into calculation whether the user could afford purchasing the selected laptop with a loan (any outstanding balance, earnings, loans and repayments resulting from depositing earnings are accounted for). Sensible and minimal prompts are provided along the way.

const kr = " Kr.";
const earnAmount = 100;
const loanRepaymentRateFromEarnings = 0.1;

//------//
// BANK //
//------//

/* Balance elements */
const balanceElement = document.getElementById("balance");
const balanceString = balanceElement.textContent;
let balance = Number.parseInt(balanceString.substring(0, balanceString.length - 4));

/* Loan elements */
const repayButtonElement = document.getElementById("repay");
const outstandingLoanKeyElement = document.getElementById("box-item-key-loan");
const outstandingLoanValueElement = document.getElementById("box-item-value-loan");
const outstandingLoanString = outstandingLoanValueElement.textContent;
let defaultLoanOffer = updateDefaultLoanOffer();
let outstandingLoan = Number.parseInt(outstandingLoanString.substring(0, outstandingLoanString.length - 4));

function updateDefaultLoanOffer(pay = 0) {
    return 2 * (pay + balance);
}

/* Show/hide outstanding loan elements */
onload = checkLoanElements();

function checkLoanElements() {
    if (outstandingLoan == 0) {
        repayButtonElement.style.display = "none";
        outstandingLoanValueElement.style.display = "none";
        outstandingLoanKeyElement.style.display = "none";
    } else {
        repayButtonElement.style.display = "block";
        outstandingLoanValueElement.style.display = "block";
        outstandingLoanKeyElement.style.display = "block";
    }
}

/* Get loan logic */
const loanButton = document.getElementById("loan").addEventListener("click", getloan);

function getloan(redirectedFromShop = false) {
    if (pay != 0 && !redirectedFromShop) {
        const depositEarnings = confirm(
            "You have undeposited earnings.\n" +
            "Would you like to deposit it in order to get a higher loan?"
        );
        if (depositEarnings) {
            deposit();
        }
    } else {
        deposit();
    }

    defaultLoanOffer = updateDefaultLoanOffer();
    let loan = prompt("Please enter the amount you wish to borrow:", defaultLoanOffer);

    if (loan != null) {
        loan = Number.parseInt(loan);
        if (Number.isNaN(loan)) {
            alert("Invalid input");
        } else if (outstandingLoan != 0) {
            alert(
                "There is a current loan that has not been paid off yet.\n" +
                "A request for a new loan cannot be considered before the outstanding loan is paid off."
            );
        } else if (loan <= 2 * balance) {
            outstandingLoan = loan;
            balance += loan;
            outstandingLoanValueElement.innerHTML = outstandingLoan + kr;
            balanceElement.innerHTML = balance + kr;
            checkLoanElements();
        } else {
            alert(
                `The loan request has been denied.
                The loan cannot exceed double the amount of account balance`
            );
        }
    }
}

//------//
// WORK //
//------//

/* Earn elements */
const payElement = document.getElementById("pay");
const payString = payElement.textContent;
let pay = Number.parseInt(payString.substring(0, payString.length - 4));

/* Earn logic */
const earnButton = document.getElementById("earn").addEventListener("click", earn);

function earn() {
    pay += earnAmount;
    payElement.innerHTML = pay + kr;
}

/* Deposit logic */
const depositButton = document.getElementById("deposit").addEventListener("click", deposit);

function deposit() {
    if (outstandingLoan > 0) {
        const repaymentFromEarnings = loanRepaymentRateFromEarnings * pay;
        if (outstandingLoan < repaymentFromEarnings) {
            pay -= outstandingLoan;
            outstandingLoan = 0;
            checkLoanElements();
        } else {
            pay -= repaymentFromEarnings;
            outstandingLoan -= repaymentFromEarnings;
            checkLoanElements();
        }
    }

    balance += pay;
    pay = 0;
    outstandingLoanValueElement.innerHTML = outstandingLoan + kr;
    balanceElement.innerHTML = balance + kr;
    payElement.innerHTML = pay + kr;
}

/* Repay loan logic */
const repayButton = repayButtonElement.addEventListener("click", repay);

function repay() {
    if (pay > outstandingLoan) {
        pay -= outstandingLoan;
        outstandingLoan = 0;
        checkLoanElements();
    } else {
        outstandingLoan -= pay;
        pay = 0;
    }
    payElement.innerHTML = pay + kr;
    outstandingLoanValueElement.innerHTML = outstandingLoan + kr;
}

//--------//
// LAPTOP //
//--------//

/* Laptop object */
class Laptop {
    constructor(name, price, features, description, pictureURI) {
        this.name = name;
        this.price = price;
        this.features = features;
        this.description = description;
        this.pictureURI = pictureURI;
    }
}

/* Collecting data for laptops */
const laptops = [];

const acerNitro5 = new Laptop(
    "Nitro 5 Gaming Laptop - AN515-44-R0DL",
    7999,
    [
        "Windows 10 Home",
        "AMD Ryzen 7 4800H processor Octa-core 2.90 GHz",
        "NVIDIA® GeForce® GTX 1650Ti with 4 GB dedicated memory",
        "15.6\" Full HD (1920 x 1080) 16:9",
        "16 GB, DDR4 SDRAM",
        "512 GB SSD"
    ],
    "Acer Nitro 5 is a stylish laptop designed for gamers. It has a powerful AMD Ryzen 7 processor and Nvidia GeForce GTX 1650Ti graphics card. Its 15.6\" Full HD screen has a refresh rate of 144 Hz, so you can play games with the highest refresh rate.",
    "resources/images/Acer Nitro 5.png"
);
laptops.push(acerNitro5);

const macbookAir2020 = new Laptop(
    "MacBook Air 2020 M1 MGN63DK/A",
    11799,
    [
        "Apple M1 chip with 8‑core CPU",
        "8‑core GPU",
        "16‑core Neural Engine",
        "8GB unified memory",
        "512GB SSD storage",
        "Retina display with True Tone",
        "Magic Keyboard",
        "Touch ID",
        "Force Touch trackpad",
        "Two Thunderbolt / USB 4 ports"
    ],
    "MacBook Air is available in silver, space gray and gold. It has a brilliant Retina display with True Tone technology, Touch ID, a backlit Magic Keyboard and a Force Touch trackpad. All in an iconic wedge-shaped cabinet made of 100% recycled aluminum. And with 11 hours of battery life, it's an all-in-one laptop that can run all day.",
    "resources/images/MacBook Air.png"
);
laptops.push(macbookAir2020);

const hpChromebook = new Laptop(
    "HP Chromebook 14A-NA0403EN",
    1899,
    [
        "Intel Celeron N4000",
        "Stylish and compact",
        "4 GB and 64 GB eMMC",
        "14\" IPS screen",
        "Intel UHD Graphics 600",
        "Chrome OS"
    ],
    "The very versatile Chromebook 14 meets all your daily needs and is ready for any challenge. This computer has B&O tuned speakers and has a long battery life, making it ideal for series and videos. The HP Chromebook 14 meets all your expectations. It is a stylish, sleek laptop. The fully integrated Google Play Store and long battery life allow you to work and enjoy all day.",
    "resources/images/HP Chromebook.png"
);
laptops.push(hpChromebook);

const lenovo = new Laptop(
    "Lenovo V14-IKB",
    3992,
    [
        "Intel Core i3-8130U processor",
        "Modern design and SD card reader",
        "8 GB RAM and 128 Gt M.2 SSD",
        "14\" Full HD screen",
        "180° swivel screen"
    ],
    "Lenovo V14 is an excellent laptop for everyday use with a clear screen and clear sound. A sleek and lightweight laptop is a great choice when you want to take your laptop with you everywhere.",
    "resources/images/Lenovo.png"
);
laptops.push(lenovo);

/* Set Features area in laptop selection box */
const featuresTableElement = document.getElementById("laptop-features");
onload = setFeaturesArea();

function setFeaturesArea() {
    let maxRows = 0;
    for (let laptop of laptops) {
        const currNrOfRows = laptop.features.length;
        if (maxRows < currNrOfRows) {
            maxRows = currNrOfRows;
        }
    }

    for (let i = 0; i < maxRows; i++) {
        const row = featuresTableElement.insertRow(i);
        const cell = row.insertCell(0);
        cell.setAttribute("id", `td-${i}`);
        cell.innerHTML = "<br>";
    }
};

/* Logic for laptop selection */
const selectBoxElement = document.getElementById("select-box");
const laptopFeaturesElement = document.getElementById("laptop-features");
const laptopImageElement = document.getElementById("laptop-image");
const laptopNameElement = document.getElementById("laptop-name");
const laptoptDescritptionElement = document.getElementById("laptop-description");
const laptopPriceElement = document.getElementById("laptop-price");
let price = 0;
let laptopName = laptopNameElement.textContent;

onload = updateSelectBox();

function updateSelectBox() {
    for (let laptopId in laptops) {
        const laptopElement = document.createElement("option");
        laptopElement.setAttribute("value", laptopId);
        laptopElement.innerHTML = laptops[laptopId].name;
        selectBoxElement.appendChild(laptopElement);
    }
    displayAndUpdateLaptopInfo(laptops[0]);
};

const selectLaptop = selectBoxElement.addEventListener("change",
    function(event) {
        clearFeaturesArea();
        const laptop = laptops[event.target.value];
        displayAndUpdateLaptopInfo(laptop);
    }
);

function clearFeaturesArea() {
    const cells = featuresTableElement.getElementsByTagName("td");
    for (let cell of cells) {
        cell.innerHTML = "<br>";
    }
}

function displayAndUpdateLaptopInfo(laptop) {
    const features = laptop.features;
    for (let i = 0; i < features.length; i++) {
        const cell = document.getElementById(`td-${i}`);
        cell.innerHTML = features[i];
    }
    laptopImageElement.setAttribute("src", laptop.pictureURI);
    laptopNameElement.innerHTML = laptop.name;
    laptoptDescritptionElement.innerHTML = laptop.description;
    laptopPriceElement.innerHTML = laptop.price + kr;
    price = laptop.price;
    laptopName = laptop.name;
}

/* Buy laptop logic */
const buy = document.getElementById("buy").addEventListener("click",
    function buy() {
        if (balance >= price) {
            balance -= price;
            balanceElement.innerHTML = balance + kr;
            alert(
                "Congratulations on your new purchase\n" +
                `${laptopName}`
            );
        } else {
            const hasEnoughFunds = outstandingLoan == 0 ? pay + balance > price : (1 - loanRepaymentRateFromEarnings) * pay + balance > price;
            if (hasEnoughFunds) {
                const executePurchase = confirm(
                    "Your account funds are insufficient. However, your earnings are sufficient\n\n" +
                    `Would you like to deposit your earnings and purchase the selected laptop ${laptopName}?`
                );
                
                if (executePurchase) {
                    deposit();
                    balance -= price;
                    balanceElement.innerHTML = balance + kr;
                    alert(
                        "Congratulations on your new purchase:\n" +
                        `${laptopName}`
                    );
                }
            } else {
                defaultLoanOffer = updateDefaultLoanOffer(pay);
                const canBorrowEnough = outstandingLoan == 0 ? pay + balance + defaultLoanOffer >= price : false;
                if (canBorrowEnough) {
                    const askForLoan = confirm(
                        "Insufficient funds! However, you could deposit your earnings and get a loan.\n\n" +
                        `Would you like to make a deposit and get a loan to purchase ${laptopName}?`
                    );

                    if (askForLoan) {
                        getloan(true);
                        buy();
                    }
                } else {
                    alert("Insufficient funds");
                }
            }
        }
    }
);